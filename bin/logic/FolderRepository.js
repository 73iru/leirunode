/**
 * Created by leiru on 14.12.14.
 */
var Q = require('q');
var path = require('path');
var Utils = require('../utils/Utils');
var http = require('http');
var fs = require('fs');
var UserRepository = require('./UserRepository.js');
var Validator = require("./Validator");
var rmdir = require('rimraf');

var model = require('../model/db/model').model;
var sequelize = require('../model/connection/sequelize_connection').sequelize;
var settings = require('../settings/Settings');


var FolderRepository = {

    createMainFolder: function (user, req, res, transaction) {

        if (user == null) {

            throw new Error('Invalid argument exception : Need user to create main folder');
        }

        return model.Folder.create(Utils.newMainFolder(), transaction)
                .then(function (folder) {

                console.log("folder created " + folder.name);
                return user.addFolder(folder, transaction);
         });

    },

    getFolderWithFiles: function (req, res, folderUUID) {

        var queryJson = {};
        var user = req.session.user;

        if (!folderUUID) {
            queryJson = {userEmail: user, name: 'MAIN' };
        } else {
            queryJson = {userEmail: user, uuid: folderUUID };
        }
        var parentUUID = folderUUID;

        var mainFolderDao;
        var filesToSend;
        var foldersToSend;
        var topFolderUuid;
        var isTop;
        var parentFolderUuid;

        model.Folder.find( {where :queryJson} ).then(function (folder) {

            if (folder) {
                mainFolderDao = folder;

                topFolderUuid = mainFolderDao.topFolderUuid ;
                isTop = topFolderUuid != null;
                parentUUID = folder.dataValues.uuid;
                parentFolderUuid = folder.dataValues.parentUuid ;

                return mainFolderDao.getFiles();
            } else {
                throw new Error("No folder found");
               // Utils.sendJSON(400, res, {message: "no folder", files: [], currentFolder: ''});
            }

        }).then(function (files) {

             if (files.length > 0) {
                filesToSend = files.map(function (item) {
                    return item.get({plain: true});
                });
            } else {
                filesToSend = [];

            }
            return model.Folder.findAll({where : {parentUuid:parentUUID}});

        }).then(function (folders) {

            if (folders && folders.length) {

                foldersToSend = folders.map(function (item) {
                    return item.get({ plain: true });
                });

            } else {
                foldersToSend = [];
            }
            Utils.sendJSON(200, res, { 
                    files: filesToSend,
                    folders : foldersToSend,
                    topFolderUuid: topFolderUuid,
                    currentFolder: parentUUID,
                    parentFolderUuid : parentFolderUuid

             });

        }).catch(function (err) {

            Utils.sendJSON(500, res, {message: "db error:" + err,
                files: [],
                folders :[],
                topFolderUuid:topFolderUuid,
                currentFolder: parentUUID,
                parentFolderUuid : parentFolderUuid
            });
        }).done();

    },
    getFolder: function (folderUUID, req) {

        var queryJson = {};
        var user = req.session.user;
        if (!folderUUID) {

            queryJson = {userEmail: user, name: 'MAIN'};
        } else {

            queryJson = {userEmail: user, uuid: folderUUID};
        }

        return model.Folder.find({where : queryJson});
    },
    saveFile: function (file, fileData, req, res, callback) {

        var deferred = Q.defer(); // create promise
        try {

            var uploadFolder = settings.rootDir + "/" + file.path;

            fs.mkdir(uploadFolder, '775', function (err) {

                if (err) {
                    deferred.reject(err);
                } else {
                    console.log(file);
                    var newPath = uploadFolder + '/' + file.name;

                    fs.writeFile(newPath, fileData, function (err) {

                        if (err) {

                            deferred.reject(err);
                        } else {
                            file.path = newPath;
                            deferred.resolve(file);
                        }
                    });
                }

            });
        } catch (err) {

            deferred.reject(err)
        }

        return deferred.promise;
    },
    uploadFile: function (req, res) {

        var repo = this;
        try {
            var parentUUID = req.params['parentUUID'];
            var uploadedFile = req.files.file;
            var fileToAdd, folderToSave;
            var constructedFile = Utils.constructFile(uploadedFile);
            var validateResults = Validator.validateFile(constructedFile);

            if (validateResults.length > 0) {

                throw new Error("File invalid:" + validateResults.join(","));
            }

            var promise = Q.denodeify(fs.readFile);

            promise(uploadedFile.path).then(function (data) {

                return repo.saveFile(constructedFile, data, req, res);
            }).then(function (savedFile) {

                fileToAdd = savedFile;
             
                return repo.getFolder(parentUUID, req);
            }).then(function (folder) {

                folderToSave = folder;
                return model.File.create(fileToAdd);
            }).then(function (file) {

                return folderToSave.addFile(file);
            }).then(function () {

                Utils.sendText(200, res, "File uploaded");
            }).catch(function (err) {

                console.log(err);
                Utils.sendText(500, res, err.message);
            }).done();


        } catch (err) {
            console.log(err);
            Utils.sendText(500, res, err.message);
        }
    },
    downloadFile : function(req,res,uploadDir,fileUuid, folderUuid) {

        var user = req.session.user;
        var fileData;

        model.Folder.find({where : {userEmail: user, uuid: folderUuid}}).then(function(folder) {

            return folder.getFiles();
        }).then(function(files) {

            files = files.map(function(item){

                         return item.get({plain: true})
                    }).filter(function(item){

                         return item.uuid == fileUuid;
                    });

            if(files.length == 1) {

                fileData = files[0];
                var promise = Q.denodeify(fs.readdir);

                return promise(uploadDir);
            } else {

                throw Error("file error");
            }
        }).then(function(files) {

            if (files != undefined && files.length != 0) {

                var filename = files[0];
                var filePath = uploadDir + "/" + filename;

                Utils.sendFile(res, filePath, filename, fileData);
            } else {
                Utils.sendJSON(401, res, {message: "No files"});
            }

        }).catch(function(err) {

            Utils.sendJSON(500, res, {message: "smth:"+ err});
        }).done();
    },
    deleteFile : function(req, res, fileUuid, folderUuid) {

          var user = req.session.user;


          model.File.destroy({where :{ uuid : fileUuid ,}, individualHooks : true})
            .then(function(count) {

                    console.log("Count deleted files :",count);

                    Utils.sendJSON(200, res, {message: "file deleted"});
              
            }).catch(function (err) {

                    Utils.sendJSON(500, res, {message: "error: " + err});

            });
    },
    deleteFolder : function(req,res, folderUuid) {
     
        model.Folder.findById(folderUuid).then(function(folder) {
            return folder.destroy();
        }).then(function() {
            Utils.sendJSON(200, res, {message: "folder deleted"});
        }).catch(function(err) {
            Utils.sendJSON(500, res, {message: "error: " + err});
        });

    },
    createNewFolder : function(userEmail,req, res, folderName,currentFolder,topFolder,transact) {

        try {
            var parentFolder;
            model.Folder.findById(currentFolder).then(function(folder) {
                parentFolder = folder;
                topFolder = topFolder || parentFolder.topFolderUuid;
                var newFolder = Utils.newFolder(folderName , userEmail, currentFolder, topFolder, transact)
               return model.Folder.create(newFolder);
            }).then(function (folder) {

                console.log(folder);
                return parentFolder.addChildFolder(folder);
            }).then(function (data) {
                console.log(data);
                Utils.sendJSON(200, res, {message: "folder created", status: "success"});
             });
        } catch(err) {
            console.log(err);
        }
    }
};
module.exports = FolderRepository;
