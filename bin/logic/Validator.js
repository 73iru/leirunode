/**
 * Created by leiru on 2015-01-13.
 */
var RoleEnum = require("../utils/RoleEnum.js");
var settings = require('../settings/Settings');
var FileTypeEnum = require('../utils/FileTypeEnum');
var utils = require('../utils/Utils.js');

var validator = {
	validateName: function(name) {

		var result = [];

		if (typeof(name) !== 'string') {

			result.push('Invalid argument exception!');
			return result;
		}

		var properName = utils.cutScripts(name);
		if (properName === '') {

			result.push('Wrong name');
		}

		return result;
	},
	validatePath: function(path) {

		var result = [];

		if (typeof(path) !== 'string') {

			result.push('Invalid argument exception!');
			return result;
		}
		if (!path.match(/^[\/\.a-zA-Z0-9\-]+$/)) {

			result.push('Wrong path');
		}

		return result;
	},
	validateSize: function(size) {
		var result = [];
		if (!size instanceof Number) {

			result.push("newSize not a Number");
		}

		if (size > settings.max_file_size) {

			result.push("File size too big");
		}

		if (size < 0) {

			result.push("File size can't be negative");
		}

		return result;
	},
	validateUuid: function(uuid) {
		var result = [];
		if (typeof(uuid) !== 'string') {

			result.push('Invalid argument exception! Uuid must be a string');
			return result;
		}

		if (!uuid.match(/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i)) {

			result.push("Wrong uuid format");
		}

		return result;
	},
	validateDate: function(date) {

		var result = [];
		if(!(date instanceof Date)){

			result.push('Wrong date');
		}
		return result;
	},
	validateFileType : function(type){

		var result = [];
		if (FileTypeEnum[type] == undefined) {

			result.push('Wrong file type');
		}
		return result;
	},
	validateEmail : function(email) {
		var result = [];
		if (typeof(email) !== 'string') {
			result.push('Invalid argument exception! typeof email === ' + typeof(email));
			return result;
		} else if (!email.match(/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/)) {
			result.push('Wrong email format!');
		}
		return result;
	},
	validatePassword : function(password) {
		var result = [];
		if (typeof(password) !== 'string') {

			result.push('Invalid argument exception!');
		} else if (!password.match(/(?=^.{8,}$)(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s)[0-9a-zA-Z!@#$%^&*()]*$/g)) {

			result.push('Password should have min 8 characters ,1 lowercase, 1 uppercase and 1 digit!');
		}
		return result;
	},
	validateRole : function(role) {
		var result = [];
		if (role !== RoleEnum.USER && role !== RoleEnum.ADMIN) {

			result.push("Wrong Role");
		}
		return result;
	},
	validateFile : function(file){
		var result = [];

		result.push.apply(result,validator.validatePath(file.path));
		result.push.apply(result,validator.validateName(file.name));
		result.push.apply(result,validator.validateUuid(file.uuid));
		result.push.apply(result,validator.validateSize(file.size));
		result.push.apply(result,validator.validateFileType(file.type));

		return result;
	},
	validateFolder: function(folder){
		var result = [];

		result.push.apply(result,validator.validateName(folder.name));
		result.push.apply(result,validator.validateUuid(folder.uuid));

		return result;
	},
	validateUser: function(user){
		var result = [];

		result.push.apply(result,
			validator.validateEmail(user.email));

		result.push.apply(result,
			validator.validatePassword(user.password));
		result.push.apply(result,
			validator.validateRole(user.role));

		return result;
	}
};

module.exports = validator;