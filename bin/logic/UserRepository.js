var Q = require('q');
var FolderRepository = require("./FolderRepository");
var Validator = require("./Validator");

var Utils = require('../utils/Utils');
var http = require('http');
var model = require('../model/db/model').model;
var sequelize = require('../model/connection/sequelize_connection').sequelize;

var UserRepository = {

	saveUser: function (user, req, res) {

		var newUser = {

			email: user.email,
			password: user.password,
			role: 0,
			insertdate: new Date()

		};
 		var transact;

		if (typeof req !== 'undefined') {
		
			model.User.find({ where: {email: newUser.email} },transact)
			.then(function(user) {

				if (!user) {

					return model.User.create(newUser,transact);
				} else {

					throw new Error( "User exist.");	
				}
			}).then(function (createdUser) {

				if (createdUser) {

					return FolderRepository.createMainFolder(createdUser,req,res,transact);
				} else {

					throw new Error( "Error while creating user");
				}
					
			}).then(function (folder) {
				if(folder){
					Utils.sendJSON(201, res, {message: "User " + newUser.email + " has been saved. Log in"});
				} else{
					throw new Error( "no folder :( ");
				}
			  	
			}).catch(function (err) {

			  Utils.sendJSON(500, res, {message: "Transacion Rollbacked: " + err});
			  console.error(err);
			}).done();


		} else {

			throw new Error('Invalid argument exception');
		}
	},
	login: function (user, req, res) {
		var repo = this;
		var validation = Validator.validateEmail(user.email);
		validation.concat(Validator.validatePassword(user.password));

		if (typeof req !== 'undefined' 
			&& validation.length == 0) {

			this.isLogged(req);

			model.User.find({ where: user }).then(function(user) {

				if (user) {
					repo.setLoggedUser(user,req);
					Utils.sendJSON(200, res, {message: "true"});
				} else {
					repo.logout(req);
					Utils.sendJSON(204, res, {message: "false"});
				}
			}).catch(function (error) {

		 		Utils.sendJSON(500, res, {message: "Error while connecting to db" + error});
		 	}).done();

		} else {

			throw new Error("Internal Error");
		}
	},
	logout: function (request) {

		request.session.isLogged = false;
		request.session.user = undefined;
	},
	isLogged: function (request) {

		if(request.session.isLogged === undefined) {

			request.session.isLogged = false;
		}

		return request.session.isLogged;
	},
	getLoggedUser: function(request) {

		return request.session.user;
	},
	setLoggedUser:function(user,request) {
		request.session.user =  user.email;
		request.session.isLogged = true;
	}
};

function foundUser(userArrayWithOneUser) {
	if(userArrayWithOneUser.length === 1){
		return userArrayWithOneUser[0];
	}
	throw new Error("Invalid Argument Exception: Should find one user");
}

module.exports = UserRepository;