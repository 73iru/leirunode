var fs = require('fs');
var settings = require('../settings/Settings');
var FileTypeEnum = require('./FileTypeEnum');

var utils = {
	sendJSON : function (code, res, json) {

        res.writeHead(code, {'Content-Type': 'application/json'});
        res.write(JSON.stringify(json));
        res.end();
    },
    sendText : function (code, res, text) {

        res.writeHead(code, {'Content-Type': 'text/plain'});
        res.write(text);
        res.end();
    },
    sendFile : function(res, filePath, filename,fileData) {

        var util = this;
        fs.stat(filePath,function(err,stat){
            if(err){
                util.sendJSON(500, res, err);
            } else {

                res.writeHead(200, {
                    'Content-Type': FileTypeEnum[fileData.type.toUpperCase()].mime,
                    'Content-Length': fileData.size,
                    'Content-disposition': 'attachment; filename=' + fileData.name
                });

                var readStream = fs.createReadStream(filePath);
                readStream.pipe(res);
            }
        });
    },
	genUUID  : function () {

		return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
			var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
			return v.toString(16);
		});
	},
	isEmpty: function (obj) {

		if (typeof obj === 'undefined' || obj === null || obj === '') {
			return true;
		}
		if (typeof obj === 'number' && isNaN(obj)) {
			return true;
		}
		if (obj instanceof Date && isNaN(Number(obj))) {
			return true;
		}
		return false;
	},
	newMainFolder : function() {
        var uid = utils.genUUID();
		return {
			name: "MAIN",
			uuid: uid,
            topFolderUuid : uid
		}
	},
    newFolder : function(folderName,userEmail,currentFolder,topFolder) {
        var uuid = utils.genUUID();
        topFolder = topFolder || uuid;
        return {
            name: folderName,
            uuid: uuid,
            parentUuid: currentFolder,
            topFolderUuid : topFolder,
            userEmail:userEmail
        }
    },
	cutScripts : function(name){

		return name.replace(/\<[^\>]*\>/g, '');
	},
 	constructFile: function(file) {
 		var uuid = this.genUUID();
        return {
            uuid: uuid,
            path: settings.uploadDir + '/' + uuid,
            name: file.originalname,
            size: file.size,
            type: file.extension.toUpperCase()
        }
    }
};



module.exports = utils;
