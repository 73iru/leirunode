/**
 * Created by leiru on 14.12.14.
 */
var fs = require('fs');
var path = require('path');
var setup = JSON.parse(fs.readFileSync(path.resolve("setup.json"),"utf8"));

var settings = {};
  /*  maxFileSize : setup.max_file_size,
	uploadDir : setup.uploadDir
};*/

for(setting in setup){
	settings[setting] = setup[setting];
}


module.exports = settings;