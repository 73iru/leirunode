var Sequelize = require('sequelize');
var rmdir = require('rimraf');
var settings = require('../../settings/Settings');
var sequelize = require('../connection/sequelize_connection').sequelize;

exports.initModel = function(){

	var User = sequelize.define('user', {

		email: {
			type: Sequelize.STRING,
			field: 'email',
			unique: true,
			primaryKey: true,
			allowNull: false
		},
		password: {
			type: Sequelize.STRING,
			field:'password',
			allowNull: false
		},
		role: {
			type: Sequelize.INTEGER,
			field:'role'
		}
	});

	var Folder = sequelize.define("folder", {

		uuid: {
			type: Sequelize.STRING,
			field: 'uuid',
			unique: true,
			primaryKey: true,
			allowNull: false
		},
		name: {
			type: Sequelize.STRING,
			field: 'name',
			allowNull: false
		},
        topFolderUuid: {
            type: Sequelize.STRING,
            field: 'topFolderUuid'
        },
        shared: {
            type: Sequelize.ARRAY(Sequelize.TEXT),
            field: 'shared'
        },
        parentUuid:{
            type: Sequelize.STRING,
            field: 'parentUuid'
        }

	});


	var File = sequelize.define("file", {

		uuid: { 
			type: Sequelize.STRING,
			field: 'uuid',
			unique: true,
			primaryKey: true,
			allowNull: false
		},
		name: {  
			type: Sequelize.STRING,
			field: 'name',
			allowNull: false
		},
		path: {  
			type: Sequelize.STRING,
			field: 'path',
			allowNull: false
		},
		size: {
			type: Sequelize.INTEGER,
			field:'size'
		},
		type: {  
			type: Sequelize.STRING,
			field: 'type'
		},

	},{
		hooks : {
            afterDestroy : function(file, fn) {

		        rmdir(settings.uploadDir +'/' + file.uuid , function(err) {

		        	if(err) {
						console.log('problem deleting file...', file.path );
		        	} else {
		        		console.log('file was deleted...', file.path );
		        	}
		        });
	           	return sequelize.Promise.resolve();
			}
        }
	});

	
	User.hasMany(Folder, {as: { singular: 'Folder', plural: 'Folders' }});

	Folder.hasMany(File, {	as : { singular: 'File', plural: 'Files' },
						 	'onDelete': 'cascade',
						    'hooks': true
						});

	Folder.hasMany(Folder, { as : { singular: 'ChildFolder', plural: 'ChildFolders' },
							foreignKey: 'parentUuid',
						 	'onDelete': 'cascade',
						    'hooks': true
						});

	return { File:File, User:User ,Folder:Folder };
}; 

exports.model = null;


