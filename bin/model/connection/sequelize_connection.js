
var Sequelize = require('sequelize');


// exports.sequelize = new Sequelize('postgres://otbrfoktopeihe:K7Yucw7XoBWQy6mT6FhqPfm6yS@' +
// 		'ec2-54-228-195-48.eu-west-1.compute.amazonaws.com:5432/dnnj4scvkmubn?ssl=true');
	

exports.sequelize = new Sequelize('dnnj4scvkmubn','otbrfoktopeihe','K7Yucw7XoBWQy6mT6FhqPfm6yS',{
	
  // custom host; default: localhost
  host: 'ec2-54-228-195-48.eu-west-1.compute.amazonaws.com',

 
  // custom protocol
  // - default: 'tcp'
  // - added in: v1.5.0
  // - postgres only, useful for heroku
  protocol: 'postgres',
 
  // disable logging; default: console.log


  //logging: true,
 

 
  // the sql dialect of the database
  // - default is 'mysql'
  // - currently supported: 'mysql', 'sqlite', 'postgres', 'mariadb'
  dialect: 'postgres',
  dialectOptions: {
        ssl: true
  },

 
  // a flag for using a native library or not.
  // in the case of 'pg' -- set this to true will allow SSL support
  // - default: false
  // native: true,
 
  // Specify options, which are used when sequelize.define is called.
  // The following example:
  //   define: {timestamps: false}
  // is basically the same as:
  //   sequelize.define(name, attributes, { timestamps: false })
  // so defining the timestamps for each model will be not necessary
  // Below you can see the possible keys for settings. All of them are explained on this page
  // define: {
  //   underscored: false
  //   freezeTableName: false,
  //   syncOnAssociation: true,
  //   charset: 'utf8',
  //   collate: 'utf8_general_ci',
  //   classMethods: {method1: function() {}},
  //   instanceMethods: {method2: function() {}},
  //   timestamps: true
  // },
 
  // similiar for sync: you can define this to always force sync for models
 // sync: { force: true },
 
  // sync after each association (see below). If set to false, you need to sync manually after setting all associations. Default: true
  syncOnAssociation: true,
 
  // use pooling in order to reduce db connection overload and to increase speed
  // currently only for mysql and postgresql (since v1.5.0)
  pool: { maxConnections: 5, maxIdleTime: 30},
 
  // language is used to determine how to translate words into singular or plural form based on the [lingo project](https://github.com/visionmedia/lingo)
  // options are: en [default], es
  language: 'en'
})
