#!/usr/bin/env node
var debug = require('debug')('FilePlatform');

var app = require('../app.js');
var fs = require('fs');

app.set('port', process.env.PORT || 3000);

app.listen(app.get('port'), function() {

	fs.mkdir('uploadDir' ,'775',function(err){
		if(err){
			console.log(err);
		}
	});
	//debug('Express server is on dev mode and listening on port ' + app.get('port'));
});

