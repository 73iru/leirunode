var utils = require('../bin/utils/Utils.js');
var express = require('express');
var settings = require('../bin/settings/Settings');

var path = require('path');

var FolderRepository = require('../bin/logic/FolderRepository');


var router = express.Router();


router.post('/:parentUUID/', function(req, res) {
	
	FolderRepository.uploadFile(req, res);
	
});




module.exports = router;

