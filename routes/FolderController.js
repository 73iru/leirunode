var express = require('express');
var utils = require('../bin/utils/Utils');
var FolderRepository = require('../bin/logic/FolderRepository');
var router = express.Router();
var settings = require('../bin/settings/Settings');
var UserRepository = require('../bin/logic/UserRepository');
var model = require('../bin/model/db/model').model;

router.get('/', function(req, res) {

    FolderRepository.getFolderWithFiles(req,res);
});

router.get('/:uuid/', function(req, res) {

    var uuid = req.params['uuid'];
    FolderRepository.getFolderWithFiles(req,res, uuid);
});

router.get('/:folderUuid/file/:fileUuid', function (req, res) {

    var fileUuid = req.params['fileUuid'];
    var folderUuid = req.params['folderUuid'];
    var uploadDir = settings.rootDir + "/" + settings.uploadDir + "/" + fileUuid;

    FolderRepository.downloadFile(req,res,uploadDir,fileUuid,folderUuid);
});

router.delete('/:folderUuid/file/:fileUuid', function (req, res) {

    var fileUuid = req.params['fileUuid'];
    var folderUuid = req.params['folderUuid'];

    FolderRepository.deleteFile(req,res,fileUuid,folderUuid);
});

router.delete('/:folderUuid/', function (req, res) {

    var folderUuid = req.params['folderUuid'];

    FolderRepository.deleteFolder(req,res,folderUuid);
});

router.post('/new' ,function(req, res) {

    var params = req.body;
    var user = UserRepository.getLoggedUser(req);
    FolderRepository.createNewFolder(user,req, res,params.folderName,params.currentFolder,params.topFolder);
});

module.exports = router;



