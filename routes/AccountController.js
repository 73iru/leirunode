var express = require('express');
var router = express.Router();
var UserRepository = require('../bin/logic/UserRepository');
var utils = require("../bin/utils/Utils");

router.get('/islogged',function(req, res){

	res.send({ message:'HI'});
});

router.all('/*', function (req, res, next) {

	if (UserRepository.isLogged(req)) {

		next();
	} else {

		utils.sendJSON(401,res, { message: "redirect" });
	}
});

router.get('/', function(req, res) {

	res.render('home.html');
});

module.exports = router;

