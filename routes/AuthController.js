var express = require('express');
var utils = require('../bin/utils/Utils');
var UserRepository = require('../bin/logic/UserRepository');


var router = express.Router();

router.get('/', function(req, res) {

	res.render('index.html');
});

router.post('/login', function(req, res) {

	getLoginAndPasswordFromRequestAndDo(req, res, function(user) {

		UserRepository.login(user, req, res);
	});
});

router.post('/create', function(req, res) {

	getLoginAndPasswordFromRequestAndDo(req, res, function(user) {

		UserRepository.saveUser(user, req, res);
	});
});


router.get('/logout', function(req,res) {
	UserRepository.logout(req);
	utils.sendJSON(200,res,{ message: "logout"});
});


 /*
 *
 * UTIL FUNCTIONS
 *
 */
function getLoginAndPasswordFromRequestAndDo(req, res, callback) {

	var params = req.body;
	var login = params.login;
	var password = params.password;

	if (utils.isEmpty(login) || utils.isEmpty(password)) {

		utils.sendJSON(204, res, { message:"Invalid agrument exceptiion : empty login or password" });
	} else {

		try {

			var user = { email: login, password: password };
			if(callback) callback.call(this,user);

		} catch (error) {

			utils.sendJSON(500, res, {message:"Internal Error"});
			console.log(error);
		}
	}
}

module.exports = router;



