myApp.service('FileService', function () {
    var service = this;

    service.myFiles = [];
    service.currentFolder = null;
    service.topFolderUuid = null;
    service.parentFolderUuid = null;

    service.maxFileSize = 2;

    service.refresh = function(){ };

    return  {

        getMyFiles : function(){
            return service.myFiles;
        },
        getCurrentFolder : function(){
            return service.currentFolder;
        },
        getMaxFileSize :function(){
            return service.maxFileSize;
        },
        getParentFolderUuid : function(){
            return service.parentFolderUuid;
        },getTopFolderUuid : function() {
            return service.topFolderUuid;
        },
        setMyFiles : function(files){
             service.myFiles = files;
        },
        setCurrentFolder : function(folderUuid){
             service.currentFolder = folderUuid;
        },
        setMaxFileSize : function(size){
             service.maxFileSize = (size / 1000); // milis to seconds
        },
        setTopFolderUuid : function(folderUuid){
            service.topFolderUuid = folderUuid;
        },
      
        setParentFolderUuid : function(uuid){
            service.parentFolderUuid = uuid;
        },
        refresh : function(){
            service.refresh();
        },
        setRefresh : function(func) {
            service.refresh = func;
        }
    }
});
