myApp.service('AuthService', ['$http', function ($http) {

	this.login = function (data) {

		if(!this.validateLoginAndPassword(data.login,data.password)) {
			return;
		}

		$http({
			url     : "login",
			data    : $.param(data),
			dataType: "json",
			method  : "POST",
			headers : {
				"Content-Type": "application/x-www-form-urlencoded"
			}
		}).success(function (data, status) {

			if (status === 200 && data.message === "true") {

				location = location + "account/";

			} else if(status === 204 && data.message === "false") {

				alert('Wrong login data');
			}
		}).error(function (data, status) {

			alert(JSON.stringify(data));
		});
	};

	this.createAndValidate = function (data) {

		if(!this.validateLoginAndPassword(data.login,data.password)) {
			return;
		}

		$http({
			url     : 'create',
			data    : $.param(data),
			dataType: "json",
			method  : "POST",
			headers : {
				"Content-Type": "application/x-www-form-urlencoded"
			}
		}).success(function (data,status) {

			if (status === 201) {

				alert("account created!");

			} else {

				alert(status + "  " +JSON.stringify(data));
			}
		}).error(function (data, status) {

				alert(JSON.stringify(data));
		});
	};



	this.logout = function () {
		$http.get( location.origin +'/logout').success(function (data) {
			if(data.message === "logout") {
				location = '../';
			}
		});
	};

	this.validateLoginAndPassword = function(login,password) {
		if(typeof(login) === 'undefined') {
			alert('Login has to be in email format');
			return false;
		}
		if(!password.match(/(?=^.{8,}$)(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s)[0-9a-zA-Z!@#$%^&*()]*$/g)) {
			alert('Password must have one Uppercase lettrer ,one digit, eight characters');
			return false;
		}
		return true;
	}

} ]);