myApp.controller('FilesCtrl', ['$scope','$rootScope', '$http', 'FileService', function ($scope,$rootScope, $http, FileService) {

    var init = function(){
        
        $scope.refreshFiles();
        FileService.setRefresh($scope.refreshFiles);
    };

    $scope.files = [];
    $scope.folders = [];
    $scope.folderName = "";
    $scope.parentFolderUuid = "";
    $scope.hasParent = false;

    $scope.createFolder = function() {
        var data = {};

        data.folderName = $scope.folderName;
        data.currentFolder = FileService.getCurrentFolder();
        data.topFolder = FileService.getTopFolderUuid();

        $http({
                url     : '/account/folder/new',
                data    : $.param(data),
                dataType: "json",
                method  : "POST",
                headers : {
                    "Content-Type": "application/x-www-form-urlencoded"
                }
            }).success(function(data) {

                $scope.refreshFiles();
            });
    };


    $scope.refreshFiles = function() {

        var folderUuid = FileService.getCurrentFolder()? FileService.getCurrentFolder():'';

        $http.get('/account/folder/' + folderUuid).success(function (data) {

                FileService.setMyFiles(data.files);

                FileService.setCurrentFolder(data.currentFolder);
                FileService.setTopFolderUuid(data.topFolderUuid);
                FileService.setParentFolderUuid(data.parentFolderUuid);
                
                $scope.hasParent = (data.parentFolderUuid != null && data.parentFolderUuid != undefined) ;

                $scope.folders = data.folders;
                $scope.files = data.files;
                $scope.brodcastReady();
            });
    };

    $scope.folderForm = false;

    $scope.toggleFolderForm = function() {
        $scope.folderForm = !$scope.folderForm;
    };

    $scope.brodcastReady = function(){
         $rootScope.$broadcast('FileService.ready');
    }

    $scope.changeFolder = function(uuid) {
        FileService.setCurrentFolder(uuid);
        $scope.refreshFiles();
    };

	$scope.files = FileService.myFiles;

    $scope.downloadFile = function(file) {
        window.open(location.origin + '/account/folder/'+ FileService.getCurrentFolder() +'/file/' + file.uuid);
        $scope.refreshFiles();
    };

    $scope.deleteFile = function(file) {
        if(confirm("Usunąć plik ? " + file.name)){
            $http.delete(location.origin + '/account/folder/'+ FileService.getCurrentFolder() +'/file/' + file.uuid)
                .success(function () {
                    $scope.refreshFiles();
                });
        }
    };

    $scope.deleteFolder = function(folder) {
        if(confirm("Usunąć folder ? " + folder.name)) {
            $http.delete(location.origin + '/account/folder/'+ folder.uuid)
                .success(function () {
                    $scope.refreshFiles();
                });
        }
    };

    $scope.upFolder = function(){
        $scope.changeFolder(FileService.getParentFolderUuid());
    }

    init();

}]);
