myApp.controller('HeaderCtrl', ['AuthService', '$http', '$scope', function (AuthService, $http, $scope) {

	var init = function () {
		$http.get("/data/i18n.json").success(function (data) {
				window.myLocaleJson = data;
			});
	};

	$scope.logout = function () {
		AuthService.logout();
	};

	init();

}]);