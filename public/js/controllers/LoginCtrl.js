myApp.controller("LoginCtrl", ['AuthService', '$http', '$scope', function (AuthService, $http, $scope) {

	$scope.createAndValidate = false;
	$scope.message = undefined;
	$scope.model = {};

	$scope.login = function () {

		console.log($scope.model);
		AuthService.login($scope.model);
	};

	$scope.signIn = function () {

		console.log($scope.model);
		AuthService.createAndValidate($scope.model);
	};

	$('#myModal').modal({
			"backdrop": "static",
			"keyboard": false
		});


}]);
