myApp.controller('UploadCtrl',["FileService","$scope","$http", function (FileService,$scope,$http) {

    var myDropzone = null; 
    var dropzoneInit = function(){

        if(!myDropzone) {

           myDropzone = new Dropzone(".upload",{

                url: "/account/upload/"+ FileService.getCurrentFolder() +"/",
                createImageThumbnails: true,
                dictDefaultMessage: '',
                addRemoveLinks:true,
                maxFilesize: 2,
                init: function () {
                    this.on("complete", function (file) {

                        var dropzone = this;
                        if (dropzone.getUploadingFiles().length === 0 
                            && dropzone.getQueuedFiles().length === 0) {

                            FileService.refresh();
                        }

                        if(file.status === 'success') {

                            setTimeout(function(){ dropzone.removeFile(file) },1500);
                        }

                    });
                }
            });
        } else { 
            myDropzone.options.url = "/account/upload/"+ FileService.getCurrentFolder() +"/";
        }
        
    };

    $scope.$on('FileService.ready', function() {

       dropzoneInit();
    });

}]);
