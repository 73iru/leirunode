var myApp = angular.module('myApp', ['ngRoute', 'ngAnimate']);

myApp.config([ '$routeProvider', function ($route) {
	$route.when("/", { templateUrl: "/view/main_page.html" })
        .when("/Show", { templateUrl: "/view/files_table.html" })
        .otherwise({template: '<div class="alert alert-warning">404 mate ! Can\'t find that url !</div>'});

}]);


myApp.config(['$httpProvider', function ($httpProvider) {
	$httpProvider.interceptors.push(function ($q) {
		return {
			'response'     : function (response) {

				//Will only be called for HTTP up to 300
				return response;
			},

			'responseError': function (rejection) {

				if (rejection.status === 401) {

					location = '/';
				}
				return $q.reject(rejection);
			}
		};
	});
}]);




