/**
 * Module dependencies.
 */
'use strict';

var express = require('express');
var path = require('path');
var serveFavicon = require('serve-favicon');
var multer = require('multer');
var dbModel = require('./bin/model/db/model');
var bodyParser = require('body-parser');
var errorHandler = require('errorhandler');
var methodOverride = require('method-override');
var session = require('express-session');
var logger = require('morgan');
var app = express();

require('./bin/settings/Settings').rootDir = path.resolve(__dirname);

dbModel.model = dbModel.initModel();


// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.engine('html', require('ejs').renderFile);
app.use(logger('dev'));
app.use(serveFavicon('favicon.ico'));
app.use(session({ resave    : true,
	saveUninitialized       : true,
	secret                  : 'uwotm8somethingsomething' }));
app.use(bodyParser.json());
app.use(multer());
app.use(bodyParser.urlencoded({extended: true}));
app.use(methodOverride());
app.use(bodyParser.raw());
app.use(bodyParser.text());


var auth = require('./routes/AuthController');
var account = require('./routes/AccountController');
var folder = require('./routes/FolderController');
var upload = require('./routes/UploadController');

app.use('/', auth);
app.use('/account', account);
app.use('/account/upload' , upload);
app.use('/account/folder' , folder);

app.use(express.static(path.join(__dirname, 'public')));

app.use(function (req, res, next) {
	res.status(404);

	if (req.accepts('html')) {
		res.render('404.html', { url: req.url });
		return;
	}

	if (req.accepts('json')) {
		res.send({ error: 'Not found' });
		return;
	}
	res.type('txt').send('Not found');
    next();
});

// development only
if ('development' === app.get('env')) {
	app.use(errorHandler());
}
module.exports = app;