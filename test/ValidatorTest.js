/**
 * Created by leiru on 2015-01-14.
 */
var Validator = require('../bin/logic/Validator');
var assert = require('assert');
var utils = require('../bin/utils/Utils');
var RoleEnum = require("../bin/utils/RoleEnum");
var FileTypeEnum = require('../bin/utils/FileTypeEnum');

describe('ValidatorTest', function() {

	const PROPER_EMAIL = "proper@email.pl";
	const NO_AT_EMAIL = "properemail.pl";
	const PROPER_PASSWORD = "Haslo1234";
	const BAD_PASSWORD = "haslo";
	const PROPER_NAME = "name";
	const PPROPER_UUID = utils.genUUID();



	it("validateName should validate", function(done) {

		assert.equal(Validator.validateName(1).length, 1, "validateName doesn't work");
		assert.equal(Validator.validateName(PROPER_NAME).length, 0, "validateName doesn't work");
		assert.equal(Validator.validateName('<>').length, 1, "validateName doesn't work");
		done();
	});

	it("validateEmail should validate", function(done) {

		assert.equal(Validator.validateEmail(PROPER_EMAIL).length,0, "validateEmail doesn't work");
		assert.equal(Validator.validateEmail(NO_AT_EMAIL).length, 1, "validateEmail doesn't work");
		assert.equal(Validator.validateEmail(1).length, 1, "validateEmail doesn't work");

		done();
	});

	it("validateSize should validate", function(done) {

		assert.equal(Validator.validateSize(1).length, 0, "validateSize doesn't work");
		assert.equal(Validator.validateSize(-1).length, 1, "validateSize doesn't work");
		assert.equal(Validator.validateSize(324432423432424234324324324234).length, 1, "validateSize doesn't work");
		done();
	});

	it("validatePath should validate", function(done) {

		assert.equal(Validator.validatePath(1).length, 1, "validatePath doesn't work");
		assert.equal(Validator.validatePath(PROPER_NAME).length, 0, "validatePath doesn't work");
		assert.equal(Validator.validatePath('__').length, 1, "validatePath doesn't work");

		done();
	});

	it("validatePassword should validate", function(done) {

		assert.equal(Validator.validatePassword(1).length, 1, "validatePassword doesn't work");
		assert.equal(Validator.validatePassword(PROPER_PASSWORD).length, 0, "validatePassword doesn't work");
		assert.equal(Validator.validatePassword(BAD_PASSWORD).length, 1, "validatePassword doesn't work");

		done();
	});

	it("validateUuid should validate", function(done) {

		assert.equal(Validator.validateUuid(PPROPER_UUID+1).length, 1, "validateUuid doesn't work");
		assert.equal(Validator.validateUuid(PPROPER_UUID).length, 0, "validateUuid doesn't work");
		assert.equal(Validator.validateUuid(1).length, 1, "validateUuid doesn't work");

		done();
	});



	it("validateUser should validate", function(done) {


		assert.equal(Validator.validateUser({email: NO_AT_EMAIL, password: PROPER_PASSWORD, role: RoleEnum.ADMIN , insertdate : new Date()}).length, 1, "validateUser1 doesn't work");
		assert.equal(Validator.validateUser({email: PROPER_EMAIL, password: "wertyui1", role: RoleEnum.USER, insertdate : new Date()}).length, 1, "validateUser2 doesn't work");
		assert.equal(Validator.validateUser({email: PROPER_EMAIL, password: PROPER_PASSWORD, role: RoleEnum.USER }).length, 1, "validateUser3 doesn't work");
		assert.equal(Validator.validateUser({email: PROPER_EMAIL, password: PROPER_PASSWORD, role: -2323, insertdate : new Date()}).length, 1, "validateUser4 doesn't work");

		done();
	});

	it("validateFile should validate", function(done) {

		assert.equal(Validator.validateFile({path: "__", name: PROPER_NAME, uuid: PPROPER_UUID , size : 2,type: FileTypeEnum.MPG.type ,insertdate:new Date()}).length, 1, "validateFile doesn't work");
		assert.equal(Validator.validateFile({path: 1, name: PROPER_NAME, uuid: PPROPER_UUID+1 , size : 2,type:FileTypeEnum.MPG.type,insertdate:new Date()}).length, 2, "validateFile doesn't work");
		assert.equal(Validator.validateFile({path: PROPER_NAME, name: PROPER_NAME, uuid: PPROPER_UUID , size :-2,type:FileTypeEnum.MPG.type,insertdate:new Date()}).length, 1, "validateFile doesn't work");
		assert.equal(Validator.validateFile({path: PROPER_NAME, name: PROPER_NAME, uuid: PPROPER_UUID , size : 2,type:"d", insertdate: new Date()}).length, 1, "validateFile doesn't work");
		assert.equal(Validator.validateFile({path: PROPER_NAME, name: PROPER_NAME, uuid: PPROPER_UUID , size : 2,type:FileTypeEnum.CAT,insertdate:2}).length, 2, "validateFile doesn't work");

		done();
	});

	it("validateFolder should validate", function(done) {

		assert.equal(Validator.validateFolder({name: PROPER_NAME, uuid: PPROPER_UUID,  insertdate : 2}).length, 1, "validateFolder1 doesn't work");
		assert.equal(Validator.validateFolder({name: 0, uuid: PPROPER_UUID,  insertdate : new Date() }).length, 1, "validateFolder2 doesn't work");
		assert.equal(Validator.validateFolder({name: PROPER_NAME, uuid: PPROPER_UUID+1,  insertdate : new Date()}).length, 1, "validateFolder3 doesn't work");

		done();
	});



});