/**
 * Created by leiru on 13.12.14.
 */
var ARFolder = require('../bin/logic/ARFolder.js');
var assert = require('assert');
var utils = require('../bin/utils/Utils');
var RoleEnum = require("../bin/utils/RoleEnum");

describe('ARFolderTest', function() {

    const PROPER_NAME = "NameWITHDigits123543_";
    const BAD_UUID = "wq3f3ffew-D332-4ewr4-123456789012";

    var folder;

    before(function() {
        var fold = {
            name:"name",
            insertdate : new Date(),
            uuid: utils.genUUID()
        };
	    var usr = {
		    email: "222@22.pl",
		    password: "Haslo1234",
		    role: RoleEnum.ADMIN
	    };
        fold.getOwner = function(){};
        fold.setOwner = function(){};

      folder = new ARFolder(fold);
    });

    it("name should be ok", function(done) {
        folder.changeName(PROPER_NAME);
        assert(folder.getName() == PROPER_NAME);

        done();
    });

    it("name shouldn't be ok", function(done) {

        assert.throws(function() {
            folder.changeName("<script></script>");
        }, Error, "Error thrown");


        folder.changeName("<script> function(){ alert('You are hacked!') }</script>");
        assert.equal(folder.getName() , " function(){ alert('You are hacked!') }","shiet");

        done();
    });

    it("name shouldn't be ok 2", function(done) {

        assert.throws(function() {
            folder.changeName(0);
        }, Error, "Error thrown");

        assert.throws(function() {
            folder.changeName(new Date());
        }, Error, "Error thrown");

        assert.throws(function() {
            folder.changeName(new Number.MAX_VALUE);
        }, Error, "Error thrown");

        assert.throws(function() {
            folder.changeName({newName : "newName"});
        }, Error, "Error thrown");

        assert.throws(function() {
            folder.changeName(Number.NaN);
        }, Error, "Error thrown");


        assert("name should be string", folder.changeName("e"));

        done();
    });


    it("test insertdate", function(done) {
        var date = folder.getInsertDate();


        assert("insetdate is not a date", date instanceof Date );

        done();
    });


});
