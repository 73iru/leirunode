var ARUser = require('../bin/logic/ARUser.js');
var assert = require('assert');
var RoleEnum = require("../bin/utils/RoleEnum");


describe('ARUserTest', function() {

	const PROPER_EMAIL = "proper@email.pl";
	const NO_AT_EMAIL = "properemail.pl";
	const PROPER_PASSWORD = "Haslo1234";

	var juzek;

	before(function() {
		var user = {
			email:PROPER_EMAIL,
			password :PROPER_PASSWORD,
			role : RoleEnum.USER,
			insertdate:new Date()
		};
		user.getFolders = function(){};

		juzek = new ARUser(user);
	});

	it("email type shouldn't be ok", function(done) {
		assert.throws(function() {
			juzek.changeEmail(NO_AT_EMAIL);
		}, Error, "Error thrown");
		assert.throws(function() {
			juzek.changeEmail(2);
		}, Error, "Error thrown");

		done();
	});

	it('email should be ok', function(done) {
		juzek.changeEmail(PROPER_EMAIL);
		assert.equal(juzek.getEmail(), PROPER_EMAIL);
		done();
	});


	it("password format sgould be only a string", function(done) {
		assert.throws(function() {
			juzek.changePassword(NO_AT_EMAIL);
		}, Error, "Error thrown");
		assert.throws(function() {
			juzek.changePassword("e");
		}, Error, "Error thrown");
		assert.throws(function() {
			juzek.changePassword(4);
		}, Error, "Error thrown");
		assert.throws(function() {
			juzek.changePassword("WWWWWWWWWWWWWWWWWWWWW");
		}, Error, "Error thrown");

		done();
	});

	it('password should be ok', function(done) {
		juzek.changePassword(PROPER_PASSWORD);
		assert.equal(juzek.getPassword(), PROPER_PASSWORD);
		done();
	});

	it("role should be 0 or 1", function(done) {
		assert.throws(function() {
			juzek.changeRole(-1);
		});
		assert.throws(function() {
			juzek.changeRole(3);
		});
		assert.throws(function() {
			juzek.changeRole(3567457574567);
		});

		done();
	});

	it('role should be ok', function(done) {
		juzek.changeRole(RoleEnum.USER);
		assert(juzek.getRole() === RoleEnum.USER, "Role isn't USER");
		done();
	});

	it("User should be created", function(done) {

		var user = ARUser.prototype.createAndValidate({email: PROPER_EMAIL, password: PROPER_PASSWORD, role: 1,insertdate : new Date()});

		assert.equal(user.getEmail(), PROPER_EMAIL, "email is wrong");
		assert.equal(user.getRole(), RoleEnum.ADMIN, "Role is not ADMIN");
		assert.equal(user.getPassword(), PROPER_PASSWORD, "Password is wrong");

		done();
	});

	it('create should throw exceptions', function(done) {
		assert.throws(function() {
			ARUser.prototype.createAndValidate({email: NO_AT_EMAIL, password: PROPER_PASSWORD,role: RoleEnum.ADMIN}) });
		assert.throws(function() {
			ARUser.prototype.createAndValidate({email: PROPER_EMAIL, password: "wertyui1", role: RoleEnum.USER})	});
		assert.throws(function() {
			ARUser.prototype.createAndValidate({email: PROPER_EMAIL, password: PROPER_PASSWORD, role: -2323})	});

			done();
		});
	});